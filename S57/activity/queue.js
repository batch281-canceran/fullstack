let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection;
}

function enqueue(element) {
    let newArray = [];
    let length = size();
    for (let i = 0; i < length; i++) {
        newArray[i] = collection[i];
    }
    newArray[length] = element;
    collection = newArray;

    return print();
}


function dequeue() {
    if (size() === 0) {
        return 'Queue is empty';
    }

    let newArray = [];
    for (let i = 1; i < size(); i++) {
        newArray[i - 1] = collection[i];
    }
    collection = newArray;

    return print();
}



function front() {
    // Check if the collection array is empty
    if (isEmpty()) {
        return "Queue is empty";
    }
    
    // Return the first element of the collection array
    let firstElement;
    for (let i = 0; i < size(); i++) {
        if (i === 0) {
            firstElement = collection[i];
        }
    }
    
    return firstElement;
}

function size() {
    let count = 0;
    for (let i in collection) {
        if (collection.hasOwnProperty(i)) {
            count++;
        }
    }
    return count;
}


function isEmpty() {
    for (let key in collection) {
        if (collection.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};