function countLetter(letter, sentence) {
    let result = 0;
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    if (typeof letter !== "string" || letter.length !== 1 ) {
        return undefined;
    } else {
    // Loop 
    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            result++;
        }
    }
}
    return result; 
}
// letter , sentence , result , typeof letter !== "string" || letter.length !== 1 , return undefined , for (let i = 0; i < sentence.length; i++) , sentence[i] === letter , result++ , return result


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    const charSet = new Set();
    const loweredCasedText = text.toLowerCase();
    for (let i = 0; i < loweredCasedText.length; i++) {
        const char = loweredCasedText[i];
        if (charSet.has(char)) {
            return false;
        }
        charSet.add(char);
    }
    return true;   
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
     if (age < 13) {
        return undefined;
    }
    if (age >= 13 && age <= 21) {
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2);
    }
    if (age >= 22 && age <= 64) {
        const roundedPrice = Math.round(price);
        return roundedPrice.toFixed(2);
    }
    return undefined;
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    const hotCategories = new Set();
    for (const item of items) {
        if (item.stocks === 0) {
            hotCategories.add(item.category);
        }
    }
    const hotCategoriesArray = Array.from(hotCategories);
    return hotCategoriesArray;
}

function findFlyingVoters(candidateA, candidateB) {
    const flyingVoters = [];
    for (let i = 0; i < candidateA.length; i++) {
        if (candidateB.includes(candidateA[i])) {
            flyingVoters.push(candidateA[i]);
        }
    }
    return flyingVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};