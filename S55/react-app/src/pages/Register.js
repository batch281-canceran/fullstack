// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import {Button, Form, Row, Col} from 'react-bootstrap';
//we need to import the useState from the react
import {useState, useEffect, useContext} from 'react';

import {useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import PageNotFound from './PageNotFound';

export default function Register(){
	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [isPassed, setIsPassed] = useState(true);

	const [isDisabled, setIsDisabled] = useState(true);

	//we are going to add/create a state that will declare whether the password1 and password 2 is equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true); 
 
 	//when the email changes it will have a side effect that will console its value
	useEffect(()=>{
		if(email.length > 15){
			setIsPassed(false);
		}else{
			setIsPassed(true);
		}
	}, [email]);

	//this useEffect will disable or enable our sign up button
	useEffect(()=> {
		//we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button.
		if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && email.length <= 15){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [email, password1, password2]);

	//function to simulate user registration
	function registerUser(event) {
		//prevent page reloading
		event.preventDefault();

		alert('Thank you for registering!');

		navigate('/login');
		
	}

	//useEffect to validate whether the password1 is equal to password2
	useEffect(() => {
		if(password1 !== password2){
			setIsPasswordMatch(false);
		}else{
			setIsPasswordMatch(true);
		}

	}, [password1, password2]);

	return(
		user.id === null || user.id === undefined
		?
		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Register</h1>
				<Form onSubmit ={event => registerUser(event)}>
				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	placeholder="Enter email" 
				        	value = {email}
				        	onChange = {event => setEmail(event.target.value)}
				        	/>
				        <Form.Text className="text-danger" hidden = {isPassed}>
				          The email should not exceed 15 characters!
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password" 
				        	value = {password1}
				        	onChange = {event => setPassword1(event.target.value)}
				        	/>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Retype your nominated password" 
				        	value = {password2}
				        	onChange = {event => setPassword2(event.target.value)}
				        	/>

				        <Form.Text className="text-danger" hidden = {isPasswordMatch}>
				          The passwords does not match!
				        </Form.Text>
				      </Form.Group>

				      

				      <Button variant="primary" type="submit" disabled = {isDisabled}>
				        Sign up
				      </Button>
				</Form>
			</Col>
		</Row>
		:
		<PageNotFound/>
		)
}
