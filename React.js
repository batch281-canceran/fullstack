/* React 
- Start a New React Project
If you want to build a new app or a new website fully with React, we recommend picking one of the React-powered frameworks popular in the community. Frameworks provide features that most apps and sites eventually need, including routing, data fetching, and generating HTML.

Note
You need to install Node.js for local development. You can also choose to use Node.js in production, but you don’t have to. Many React frameworks support export to a static HTML/CSS/JS folder.

Production-grade React frameworks 
Next.js 
Next.js is a full-stack React framework. It’s versatile and lets you create React apps of any size—from a mostly static blog to a complex dynamic application. To create a new Next.js project, run in your terminal: */ 

 // Terminal
//  Copy
/* npx create-next-app
If you’re new to Next.js, check out the Next.js tutorial.

- Next.js 
is maintained by Vercel. You can deploy a Next.js app to any Node.js or serverless hosting, or to your own server. Fully static Next.js apps can be deployed to any static hosting.

- Remix 
Remix is a full-stack React framework with nested routing. It lets you break your app into nested parts that can load data in parallel and refresh in response to the user actions. To create a new Remix project, run:

 // Terminal
 // Copy
npx create-remix
If you’re new to Remix, check out the Remix blog tutorial (short) and app tutorial (long).

/* Remix is maintained by Shopify. When you create a Remix project, you need to pick your deployment target. You can deploy a Remix app to any Node.js or serverless hosting by using or writing an adapter.

- Gatsby 
Gatsby is a React framework for fast CMS-backed websites. Its rich plugin ecosystem and its GraphQL data layer simplify integrating content, APIs, and services into one website. To create a new Gatsby project, run:

 // Terminal
//  Copy

- npx create-gatsby
If you’re new to Gatsby, check out the Gatsby tutorial.

- Gatsby 
is maintained by Netlify. You can deploy a fully static Gatsby site to any static hosting. If you opt into using server-only features, make sure your hosting provider supports them for Gatsby.

- Expo (for native apps) 
Expo is a React framework that lets you create universal Android, iOS, and web apps with truly native UIs. It provides an SDK for React Native that makes the native parts easier to use. To create a new Expo project, run:

 // Terminal
 // Copy

- npx create-expo-app
If you’re new to Expo, check out the Expo tutorial.

- Expo 
is maintained by Expo (the company). Building apps with Expo is free, and you can submit them to the Google and Apple app stores without restrictions. Expo additionally provides opt-in paid cloud services.

// DEEP DIVE
- Can I use React without a framework? 

// Show Details
/* Bleeding-edge React frameworks 
As we’ve explored how to continue improving React, we realized that integrating React more closely with frameworks (specifically, with routing, bundling, and server technologies) is our biggest opportunity to help React users build better apps. The Next.js team has agreed to collaborate with us in researching, developing, integrating, and testing framework-agnostic bleeding-edge React features like React Server Components.

These features are getting closer to being production-ready every day, and we’ve been in talks with other bundler and framework developers about integrating them. Our hope is that in a year or two, all frameworks listed on this page will have full support for these features. (If you’re a framework author interested in partnering with us to experiment with these features, please let us know!) */

/* 
- Next.js (App Router) 
Next.js’s App Router is a redesign of the Next.js APIs aiming to fulfill the React team’s full-stack architecture vision. It lets you fetch data in asynchronous components that run on the server or even during the build.

Next.js is maintained by Vercel. You can deploy a Next.js app to any Node.js or serverless hosting, or to your own server. Next.js also supports static export which doesn’t require a server. */ 

/* returning results back to FRONTEND 
- you typically use an HTTP response returning result from a server-side script to the front end using web framework 
like Express.js */
Ex:
// server.js (Node.js with Express.js)
const express = require('express');
const app = express();
// Define a route handler
app.get('/api/data', (req, res) => {
  // Process the data or perform any necessary operations
  const result = { message: 'Hello, world!' };
  // Send the result back to the front end
  res.json(result);
});
// Start the server
app.listen(3000, () => {
  console.log('Server started on port 3000');
});
// from GET reqest made to /api/data endpoint and the result a JSON object with a "message" property
// On the front end, you can make a request to the server endpoint using JavaScript's fetch API or any other library/framework of your choice to retrieve the result:
Ex: 
// client.js (Front-end JavaScript)
fetch('/api/data')
  .then(response => response.json())
  .then(data => {
    // Process the returned result
    console.log(data.message);
  })
  .catch(error => {
    // Handle any errors that occur during the request
    console.error('Error:', error);
  });
// front-end script makes a GET request to the /api/data and retrieves the returned result using response.json()

/* Terminal runs: 
    npm install 
- reads the package.json file in the project directory and installs all the listed dependencies./ node_modules

npm test
- used when you have test files or test suites written using a testing framework such as Mocha, Jest, Jasmine, or others.
*/ 

/* How to write a function 
1. function
2. functionName - descriptive name that represents what the function does.
3. (parameter1, parameter2,..)
- placeholders for values that will be passed into the function when it is called.
4. Function logic
- where you write the code that performs the desired operations. 
5. return
-  to produce a result or output. 
6. OPTION (if, else, switch), loops (for, while)
-  functions within your function to accomplish more complex tasks. */ 
Ex 1: Syntax and example
function functionName(parameter1, parameter2, ...) {
  // Function logic goes here
  // Use the parameters to perform desired operations
  // Optionally, declare and initialize local variables

  // Return a value or perform an action (if applicable)
}

Ex 2: addNumbers
function addNumbers(num1, num2) {
  const sum = num1 + num2;
  return sum;
}

/* <span> </span> 
- used to group style text or other inline elements within a larger document.
- often used with CSS to apply styles or manipulate specific sections of text. 

 Ex 1: the word red is wrapped in span element w/ inline style att.
style - applies the CSS prop.
color: red; */ 
<p>This is a <span style="color: red;">red</span> text.</p>

// Ex 2: You can also use classes or IDs with the <span> element to apply styles defined in CSS. 
<p>This is a <span class="highlight">highlighted</span> text.</p>

/* document.querySelector 
- method that allows to select and retrieve the first element within document that matches a CSS selector.  */
// basic syntax document.querySelector
const element = document.querySelector(selector);

/* selector
- representing the CSS selector to match the desired element(s).
- The method returns the first element that matches the selector or null if no element is found.
Ex: 
- document.querySelector("#myDiv") selects the element with the id "myDiv" and assigns it to the variable divElement. */
<!DOCTYPE html>
<html>
<head>
  <title>Example</title>
</head>
<body>
  <div id="myDiv">Hello, World!</div>
  <script>
    // Select the element with id "myDiv"
    const divElement = document.querySelector("#myDiv");
    console.log(divElement.textContent); // Output: "Hello, World!"
  </script>
</body>
</html> 

/*  SPAN & DIV 
- to group and style content
    <span> </span> 
- inline element 
- used to group and apply styles to a specific section
- It does not add any line breaks and does not create a visual block on its own. */
Ex: 
<p>This is a <span style="color: red;">red</span> text.</p>


/*    <div></div>
- block-level ellement 
- to group and contain larger sections of content.
- creates a visual block and typically starts on a new line. It is often used as a container for other elements, providing structure and allowing you to apply styles or manipulate the content as a whole. */ 
Ex: 
<div>
  <h1>Title</h1>
  <p>Paragraph 1</p>
  <p>Paragraph 2</p>
</div>

/* keyup
- event that is triggered when a keyboard key is released after being pressed.
- common event used in JavaScript to perform actions or respond to user input when a key is released.*/
Ex:
<input type="text" id="myInput">
<script>
  // Get the input element
  const input = document.getElementById('myInput');

  // Attach a keyup event listener
  input.addEventListener('keyup', function(event) {
    // Perform actions when a key is released
    console.log('Key released!');
    console.log('Key code:', event.keyCode);
    console.log('Key value:', event.key);
  });
</script>

/* target
- refers to the element on which the event was originally triggered.
- represents the DOM element that was the source of the event.
-  You can access this property to interact with the specific element that triggered the event. */
Ex: 
<button id="myButton">Click me</button>
<script>
  // Get the button element
  const button = document.getElementById('myButton');
  // Attach a click event listener
  button.addEventListener('click', function(event) {
    // Access the target element
    const targetElement = event.target;
    // Manipulate the target element
    targetElement.textContent = 'Clicked!';
  });
</script>

/* querySelector("#txt-first-name")
- using the querySelector method to select an HTML element with the ID of txt-first-name.
*/
Ex: 
const firstNameInput = document.querySelector("#txt-first-name");
/* //firstNameInput will hold a reference to the HTML element with the ID txt-first-name
// You can then use this reference to perform various operations on the element, such as retrieving its value, modifying its content, or attaching event listeners */
Ex: 
<input type="text" id="txt-first-name" placeholder="First Name">

/* CSS properties
    common properties: 
1. color: 
2. font-size
3. background-color
4. width and height
5.  margin and padding -> top, right, bottom, left
* MARGIN -> margin-top, margin-right, margin-bottom, and margin-left.
* PADDING
- Controls the spacing around an element.
-> (padding-top, padding-right, padding-bottom, and padding-left)
- is the space inside an element's border. It creates space between the element's content and its border. 
6. border -> thickeness, style and color
7. display -> block, inline, flex
- Specifies how an element is displayed
8. position -> relative , absolute
-  Sets the positioning method of an element. 
9. text-align -> left, center, right
- Aligns the text content within an element 
10. float 
-Positions an element to the left or right of its containing element.*/

/* CSS methods
-  methods or techniques that can be used to style and manipulate elements.
commonly used : 
1. Selector
- used to target specific elements in the HTML document.
2. Property
- used to specify the visual style of an element.
3. Value
- assigned to properties to define the style of an element. 
// Values can be numerical: 
10px, 2rem ,  colors red, #00ff00 , keywords (e.g., bold, italic) , even functional values (e.g., rgba(255, 0, 0, 0.5).
4. Box Model
- concept in CSS that describes how elements are rendered on the web page.
- includes properties such as width, height, padding, margin, and border, which determine the size, spacing, and positioning of elements.
5. Layout 
- techniques to control the layout of elements on a web page.
- properties like display, position, float, flexbox and grid that enable different ways to arrange and position elements.
6. Pseudo-classes and P. elements
- :hover, :focus, :first-child, ::before, and ::after.
7. Media Queries
- responsive design, where styles can be adjusted for different screen sizes and devices. */

/* Specificity
- way to determine which styles will be applied to an element when multiple conflicting styles are defined.
 is calculated based on the following factors: L - H 
1. Inline styles 
2. ID selectors #id
ave a higher specificity compared to class selectors and element selectors. 
3. Class selectors, attribute selectors, and pseudo-classes: Class selectors, attribute selectors (.)
- (e.g., [attribute]), and pseudo-classes (e.g., :hover, :focus)
4. Element selectors and pseudo-elements <element> 
- Element selectors (e.g., div, p) and pseudo-elements (e.g., ::before, ::after */

// forms
<input type="text">
<input type="checkbox">
<input type="radio">
<input type="button">
<input type="submit">
<input type="file">

// adding items into an array
let post []; // initialize/ing empty array
let count = 1; // Initialize a variable "count" with the value of 1

post.push({ title: "First Post", content: "Lorem ipsum dolor sit amet." });
post.push({ title: "Second Post", content: "Lorem ipsum dolor sit amet consectetur." });
// Access individual posts
console.log(post[0]);  // Output: 
console.log(post[1]); 


// To BE CONTINUE
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
  e.preventDefault();  // Prevent the default form submission behavior.

  // Get the input values from the form
  const titleInput = document.querySelector("#post-title").value;
  const bodyInput = document.querySelector("#post-body").value;

  // Create a new post object and add it to the post array
  const newPost = {
    id: count,
    title: titleInput,
    body: bodyInput
  };
  post.push(newPost);

  // Increment the count variable
  count++;

  // Call the showPost function to display the updated posts
  showPost(posts);

  alert("Successfully Added");
});


// HOSTING




//  JavaScript - Reactive DOM with JSON (explanation codes)
let posts = [];
let count = 1;

// Add post data

document.querySelector("#form-add-post").addEventListener('submit', (e) =>{
    e.preventDefault();

    posts.push({
        id : count,
        title : document.querySelector('#txt-title').value,
        body : document.querySelector('#txt-body').value
    });

    count++;

    showPosts(posts);
    alert('Successfully Added');
});

// Show posts

const showPosts = (posts) => {
    let postEntries = '';

    // We are using the forEach method to iterate every post in our website. So that the website will show our created posts one by one.
    posts.forEach((post) => {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div>
        `;
    });

    document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector(`#txt-edit-id`).value = id;
    document.querySelector(`#txt-edit-title`).value = title;
    document.querySelector(`#txt-edit-body`).value = body;
}

// Update post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault();

    for(let i = 0; i< posts.length; i++){

        if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;

            showPosts(posts);
            alert('Successfully updated.')

            break;
        }
    }
})

// Delete post.

const deletePost = (id) => {
    posts = posts.filter((post) => {
        if (post.id.toString() !== id) {
            return post;
        }
    });

    document.querySelector(`#post-${id}`).remove();
}

/*  IP ADDRESS
- unique numerical label assigned to each device connected to a computer network that uses the Internet Protocol for communication.
   Serves two main purposes: 
1. identifying the host or network interface.
2. providing the location of the device in the network.
 consists of four sets of numbers, separated by periods e.g (192.168.0.1) */

// DECLARE
//- used to define or declare a variable or constant. 

// conditional rendering
//- refers to the practice of displaying different content or components based on certain conditions or criteria.

// A. TOKENS 
//- dev. tools > Network tab > if the token is obtained during the login process, perform a login. > click request details on the right hand panel > request headers tab > Auth. , Bearer or token. 

// bearer token
- type of access token commonly used in authentication and authorization mechanisms.
* Auth 2.0.
//- alphanumeric string that is issued by an authentication server and presented by a client to access protected resources.

// Functionality
//-features and capabilities of a software application that allow it to perform specific tasks.
//- It describes what the software can do and how it behaves in response to user interactions or input.

/* Credentials examples:
1. Username & password
2. API keys - developer auth.
3. Access tokens- user auth. to validates & access resources
4. Certificates- are typically issued by a trusted authority and contain cryptographic keys.
*/

/* Refactor/ing steps: 
1. Understand the existing code > identify areas that can be improved or optimized
2. Define refactoring Goals:
- goals to achive e.g.(improving code readability, reducing code dupli. , enhance perf. , best code practices.)
3. Break down large Functions : 
- identify large functions/duplications & break down into smaller more manageable functions.
4. Remove code dupl.
5. Simplify comples logics:
6. Optimize performance
7. Test 
8. Review & iterate
9. Maintain Good doc. */

// Methods
//- reusable block of code that is associated with a class or an object & are used to encapsulate related code, improve code organization, and enable code reusability.
//- represents an action or behavior that an object can perform.
Ex: JavaScript
class Calculator {
    add(a, b) {
        return a + b;
    }
    subtract(a, b) {
        return a - b;
    }
}
// Usage
let calculator = new Calculator();
let sum = calculator.add(3, 5);
let difference = calculator.subtract(10, 4);

// (++variable) Prefix & Postfix increment (variable++)
//-  are unary operators in programming languages that increase the value of a variable by 1.
//- Prefix Increment (++variable):
The prefix increment operator first increments the value of the variable and then returns the updated value.
//Postfix Increment (variable++):
The postfix increment operator first returns the current value of the variable and then increments the value.  
//-  are unary operators in programming languages that increase the value of a variable by 1.
Ex: 
int num = 5;
int result;

// Prefix increment
result = ++num;
System.out.println("Prefix increment: " + result);  // Output: 6

// Postfix increment
result = num++;
System.out.println("Postfix increment: " + result); // Output: 6

// The value of num after increment
System.out.println("Value of num: " + num);         // Output: 7


/* How to write a function / complete steps: 
1. Determine the purpose or functionality function
2.  Identify any input parameters or argumentsthat the function requires to perform its task. 
3. Decide on the return value or output of the function , if applicable.
4.  Write the function definition, including the function name, parameters, and the code block that contains the instructions or logic of the function.
5.  Test the function */
Ex: 
// I. Create a function 
function calculateRectangleArea(length, width) {
  // Check if either length or width is negative
  if (length < 0 || width < 0) {
    return "Invalid input: Length and width must be positive numbers.";
  }
  // Calculate the area by multiplying length and width
  var area = length * width;
  // Return the calculated area
  return area;
}
// II . use or call function 
Ex: 
var area1 = calculateRectangleArea(5, 10);  // Call the function with length = 5 and width = 10
console.log(area1);  // Output: 50

var area2 = calculateRectangleArea(3.5, 4.2);  // Call the function with length = 3.5 and width = 4.2
console.log(area2);  // Output: 14.7

var area3 = calculateRectangleArea(-2, 6);  // Call the function with a negative length
console.log(area3);  // Output: "Invalid input: Length and width must be positive numbers."


// i
//- ommonly used variable especially in loop structures such as for loops. It is typically used as a loop counter or iterator.
Ex: //for loop that uses i as the loop counter:
for (var i = 0; i < 5; i++) {
  console.log(i);
}
// output: 
0
1
2
3
4

// char
//- refers to a data type that represents a single character. Characters can include letters, digits, symbols, and whitespace
Ex: // declaring and using a char variable in C:
#include <stdio.h>
int main() {
  char letter = 'A';
  printf("The character is: %c\n", letter);
  return 0;
}
// In this example, we declare a variable letter of type char and assign it the value 'A'. The %c format specifier is used in printf to print the value of the char variable.
Ex output: 
The character is: A

// framework-agnostic bleeding edge 
//- means that the software or technology is not dependent on a particular framework or platform  flexibility and allowing developers to choose the tools and technologies that best suit their needs.

// Server Technologies






